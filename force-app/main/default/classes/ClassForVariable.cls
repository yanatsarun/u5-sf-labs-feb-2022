public with sharing class ClassForVariable {

    public static final Integer checkConstant = 4; 

    public ClassForVariable() {
        //checkConstant = 6;
        System.debug('Value: ' + checkConstant);
    }

    public void changeValue() {
        //static final variables cannot be updated in method        
        //checkConstant = 9;
    }

}
