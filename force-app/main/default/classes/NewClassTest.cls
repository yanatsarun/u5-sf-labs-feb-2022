@isTest
private class NewClassTest {
    @isTest(SeeAllData = true)
    static void task() {
        Account account = [SELECT Id, Name FROM Account WHERE Name = 'Jack'];
        System.assert(account != null);
        Account account2 = account.clone();
        Account account3 = [SELECT Id, Name FROM Account WHERE Name = 'Maksim'];
        account2.Name = 'Lena';
        account.Name = 'Kate';        
        update account;
        insert account2;
        delete account3;
    }
}
