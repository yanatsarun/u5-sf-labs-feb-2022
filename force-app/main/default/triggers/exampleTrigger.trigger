trigger ExampleTrigger on Account (before insert) {
    List<Account> accounts = [SELECT Id, Name FROM Account WHERE Id IN :Trigger.New];
    for(Integer i = 0; i < accounts.size(); i++) {
        if(accounts.get(i).Name == 'Dasha') {
            Trigger.New[i].addError('The error with name was occured');
        	Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { 'yana210901@gmail.com', 'dzmitry.stsepanyuk@itechart-group.com'};
            message.optOutPolicy = 'FILTER';
            message.subject = 'Subject Text Message';
            message.plainTextBody = 'This error was occured in record with Id: ' + accounts.get(i).Id;
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
			Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        }
    }
    List<Opportunity> opportunityList = new List<Opportunity>();
    for (Integer i = 0; i < accounts.size(); i++) {
        if (Trigger.New[i].hasErrors()) {
            opportunityList.add(new Opportunity(
                Name = 'New opportunity for ' + accounts.get(i).Name,
                StageName='Closed Won',
                CloseDate=System.today().addMonths(1),
                AccountId=accounts.get(i).Id
            )); 
        }
    }
}